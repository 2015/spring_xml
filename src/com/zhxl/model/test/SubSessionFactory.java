package com.zhxl.model.test;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Component;

@Component(value="subSessionFactory")
public class SubSessionFactory extends LocalSessionFactoryBean{
	@Autowired
	@Qualifier(value="dataSource")
	DataSource dataSource;
	
	
	public SubSessionFactory(){
		super();
	}
}

