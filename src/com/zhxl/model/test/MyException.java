package com.zhxl.model.test;

public class MyException extends NullPointerException {
	private static final long serialVersionUID = 1L;
	
	public MyException() {
		super();		
	}
	public MyException(String message){
		super(message);
	}
}
