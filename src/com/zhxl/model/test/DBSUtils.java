package com.zhxl.model.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class DBSUtils {
	public static List<String> lists = new ArrayList<String>();
	public static List<String> query(Connection conn){
		try {
			PreparedStatement ps = conn.prepareStatement("select * from monitoraction");
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				lists.add(rs.getString("actionName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lists;
		
	}

}
