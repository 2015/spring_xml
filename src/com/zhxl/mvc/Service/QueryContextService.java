package com.zhxl.mvc.Service;

import java.util.List;

import com.zhxl.model.Internal.User;

public interface QueryContextService {
	public <T> List<User> getContext();
}
