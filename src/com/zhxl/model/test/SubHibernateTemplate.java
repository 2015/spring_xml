package com.zhxl.model.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

@Component(value="subHibernateTemplate")
public class SubHibernateTemplate extends HibernateTemplate {
	@Autowired
	@Qualifier(value="subSessionFactory")
	SubSessionFactory sessionFactory;
	public SubHibernateTemplate(){
		super();
	}
}
