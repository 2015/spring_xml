package com.zhxl.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zhxl.model.Internal.User;
import com.zhxl.mvc.Service.QueryContextService;
import com.zhxl.mvc.Service.QueryContextServiceImpl;
import com.zhxl.mvc.Service.QueryContextServiceImpl2;
/**
 * 控制器方法级别的验证也是需要的否则真会出现找不到404的异常信息
 * @author Administrator
 *   <context:annotation-config />   
       <!-- 把标记了@Controller注解的类转换为bean -->     
      <context:component-scan base-package="com.zhxl.controller.Internal.impl" />  
      spring mvc 和applicationContext.xml文件重复会出现session无法注册
      juint4单独测试没有问题
      说明
 *
 */
@Controller
public class QueryContextControllerImpl {
	//
	//@Qualifier(value="queryContextServiceImpl")
	//@Resource(name="queryContextServiceImpl")
	@Autowired
    QueryContextServiceImpl queryContextServiceImpl;
	
//	public void setQueryContextServiceImpl(QueryContextServiceImpl queryContextServiceImpl){
//		this.queryContextServiceImpl = queryContextServiceImpl;
//	}
	
	@RequestMapping("/test")
	public String QueryContext(ModelMap model , HttpServletRequest request){
		List<User> users = null;
		User u = null,u2=null;
		if(queryContextServiceImpl == null){
			users = new ArrayList<User>();
		}
		else{
			users = queryContextServiceImpl.getContext();
			if(users != null && users.size()>0){
				u = (User) users.get(0);
				System.out.println("test..............................................."+u.getName());
				if(users.size()==2){
					u2 = users.get(1);
				}
			}
			else{
				users = new ArrayList<User>();
			}
		}
		if(u == null){
			u = new User();
			u.setName("huawei");
			u.setAge("18");
			u.setId("1");
			users.add(u);
		}
		if(queryContextServiceImpl!=null&&u2==null){
			request.setAttribute("haha","@Service Property Name is "+queryContextServiceImpl.getClass().getName());
		}
		else{
			if(u2!=null){
				request.setAttribute("haha","用户名是:"+u2.getName());
			}
			else
			{
				request.setAttribute("haha","@Service Property Name is NullPointException");
			}
		}
		model.addAttribute("User", users.get(0).getName()); 
		return "login";
		
	}
}
