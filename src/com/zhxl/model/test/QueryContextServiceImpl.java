package com.zhxl.model.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.zhxl.model.Internal.User;
import com.zhxl.mvc.Dao.QueryContextDaoImpl2;

@Service//(value="queryContextServiceImpl")
public class QueryContextServiceImpl{
	@Autowired//(required=false)
	//@Qualifier(value="queryContextDaoImpl2")
	QueryContextDaoImpl2 queryContextDaoImpl;
	//@Transactional
	public <T> List<User> getContext() {
		return (List<User>) queryContextDaoImpl.getContext();
	}
}
