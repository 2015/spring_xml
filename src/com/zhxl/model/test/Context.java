package com.zhxl.model.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.SessionFactoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
/**
 * 使用下面的配置，applicationContext.xml一定需要在source folder目录
 * 否则会出现找不到xml文件，无法加载application或者出现bean名异常信息
 * 解决办法需要build path --- Use as source Folder
 */

/*
 * core ioc
 * beans DI
 * spring -context -support integrating third lib
 * supported jdk8
 * allow groovy define configuration file
 * servlet 3.0
 * @AutoWired support general type annotation
 * 
 * WepApplicationContext for web application
 * 
 * xml configuration metadata with xml or annotation
 * 
 * ClassPathXmlApplicationContext
 * FileSystemXmlApplicationContext
 * 
 * configuration header beans context
 * 
 * load other xml configuration 
 * <beans>
 * <import resource="service.xml"/>
 * </beans>
 * 
 * c namespace used
 * 
 * annotation @Resource for beans or fields on setter method
 * and by default name injected
 * Resource(name="id值")
 * 
 * in WebAppclicationContext use annotation not xml
 * need to config <context:component-scan base-package="org.example"/>
 * 
 * @Qualifier("public")限定注解
 * 
 * @Bean是一個方法級別的
 * 
 * You can use the @Bean annotation in a @Configuration-annotated or in a @Component-annotated
 * @Bean define init-mthod or destory-method and so on
 * 
 */
@ContextConfiguration(locations={"classpath:applicationContext.xml"})

//使用下面的方法是肯定行的,xml文件中需要执行bean的全路径及其id值
//@ContextConfiguration(locations={"file:src/com/zhxl/resource/applicationContext.xml"})

public class Context {
	//@Autowired
	ApplicationContext con;
	
	Zhxl zx;
	@Autowired
	@Qualifier(value="sessionFactory")
	SessionFactory ss;
	
	@Test
	public void testSessionFactory(){
		//ss = (SessionFactoryImpl) con.getBean("sessionFactory");
		ConnectionProvider cp = ((SessionFactoryImpl) ss).getConnectionProvider();
		
		System.out.println(ss==null);
	}
	
	//@Test
	//测试IOC初始化前后返回值
	public void testbeanPostProcessorImpl(){
		BeanPostProcessorImplZHXL bppi = (BeanPostProcessorImplZHXL) con.getBean("beanPostProcessorImpl");
		Map<String,Object> maps = bppi.getMap();
		System.out.println(maps==null);
	}
	
	//@Test
	/**
	 * 测试抽象的属性值继承传递
	 */
	public void testExtendsPropertyValue(){
		SU test = (SU) con.getBean("su");
		System.out.println(test.toString());
	}
	
	//@Test
	public void testPropertyMapping(){
		PropertyPlaceholderConfigurer ph = (PropertyPlaceholderConfigurer) con.getBean("mapping");
		System.out.println(ph.getOrder());
	}
	
	//@Test
	//测试数据源查询数据
	public void testDBConnection(){
		BasicDataSource bds = (BasicDataSource) con.getBean("dataSource");
		try {
			Connection connnection = bds.getConnection();
			List<String> lists = DBSUtils.query(connnection);
			for(String str : lists){
				System.out.println("name---------:"+str);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//测试一
	//@Test
	public void testProperty(){
		/*
		 * 按照bean名加载"zhxl" @AutoWired  @Qualifier(value="zhxl") (默认按照类型加载) zhxl.class
		 */
		
	    zx = (Zhxl) con.getBean("zhxl");
	    
	    /*
	     * 按照类型加载，可以使用@Resource
	     *  zx = (Zhxl) con.getBean(Zhxl.class);
	     */
		System.out.println(zx.parse());
		System.out.println(zx.getId()+"----------------");
		BasicDataSource bss;
	}
	//@Before
	public void beforeFactory(){
		//BeanFactory bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		ApplicationContext bf = new ClassPathXmlApplicationContext("applicationContext.xml");
		Class <?> c = bf.getClass();
		//System.out.println(c.getName());
		//org.springframework.context.support.ClassPathXmlApplicationContext
	}

}
