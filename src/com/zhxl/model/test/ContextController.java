package com.zhxl.model.test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zhxl.mvc.controller.QueryContextControllerImpl;

@RunWith(SpringJUnit4ClassRunner.class)
/**
 * 使用下面的配置，applicationContext.xml一定需要在source folder目录
 * 否则会出现找不到xml文件，无法加载application或者出现bean名异常信息
 * 解决办法需要build path --- Use as source Folder
 */

/*
 * core ioc
 * beans DI
 * spring -context -support integrating third lib
 * supported jdk8
 * allow groovy define configuration file
 * servlet 3.0
 * @AutoWired support general type annotation
 * 
 * WepApplicationContext for web application
 * 
 * xml configuration metadata with xml or annotation
 * 
 * ClassPathXmlApplicationContext
 * FileSystemXmlApplicationContext
 * 
 * configuration header beans context
 * 
 * load other xml configuration 
 * <beans>
 * <import resource="service.xml"/>
 * </beans>
 * 
 * c namespace used
 * 
 * annotation @Resource for beans or fields on setter method
 * and by default name injected
 * Resource(name="id值")
 * 
 * in WebAppclicationContext use annotation not xml
 * need to config <context:component-scan base-package="org.example"/>
 * 
 * @Qualifier("public")限定注解
 * 
 * @Bean是一個方法級別的
 * 
 * You can use the @Bean annotation in a @Configuration-annotated or in a @Component-annotated
 * @Bean define init-mthod or destory-method and so on
 * 
 */
@ContextConfiguration(locations={"classpath:springMvc-servlet.xml","classpath:applicationContext.xml"})

//使用下面的方法是肯定行的,xml文件中需要执行bean的全路径及其id值
//@ContextConfiguration(locations={"file:src/com/zhxl/resource/applicationContext.xml"})
/*
 * <bean id ="queryContextDaoImpl" class="com.zhxl.controller.Internal.impl.Dao.QueryContextDaoImpl">
		<property name="sessionFactory" value="sessionFactory"></property>
	</bean>
	org.springframework.beans.NotWritablePropertyException: Invalid property '
	sessionFactory' of bean class [com.zhxl.controller.Internal.impl.Dao.QueryContextDaoImpl]: Bean property '
	sessionFactory' is not writable or has an invalid setter method. Does the 
	parameter type of the setter match the 
	
 */

public class ContextController {
	/*
	 * 	java.lang.IllegalArgumentException: 'sessionFactory' or 'hibernateTemplate' is required
	 */
	@Autowired(required=false)
	QueryContextControllerImpl queryContextControllerImpl;
	
	
//	@Test
//	public void testController(){
//		System.out.println(queryContextControllerImpl.getService()==null);
//		QueryContextServiceImpl service = queryContextControllerImpl.getService();
//		List<User> users = service.getContext();
//		if(users!=null&&users.size()>0){
//			for(User u : users){
//				System.out.println(u.getName());
//			}
//		}
//	}
	
	
}
