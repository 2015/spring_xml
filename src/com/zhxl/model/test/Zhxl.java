package com.zhxl.model.test;

import java.beans.ConstructorProperties;

public class Zhxl {
	public String name;
	public String id;
	//指定参数名的意思==构造方法个数==配置文件个数
	@ConstructorProperties({"name","id"})
	/*
	 * 不使用上，配置元数据文件
	 * <bean id="zhxl" class="com.zhxl.model.test.Zhxl">
       		<constructor-arg name="name" value="zhxl"/>
       		<constructor-arg name="id" value="12"/>
       </bean>
                 不写属性的set方法
	 */
	public Zhxl(String name , String id){
		this.name = name;
		this.id = id;		
	}
	@ConstructorProperties({"id"})
	public Zhxl(String id){
		System.err.println("single parameter running"+id);
		this.id = id;
	}
	//<constructor-arg name="name" value="zhangxiaolong"/>
	//	<constructor-arg name="id" value="1"/>
	/*
	 * 构造方法不是必须的
	 */
	public Zhxl(){
	}
	public String parse(){
		return "runtime";
	}
	public String getName() {
		return name;
	}
//	public void setName(String name) {
//		this.name = name;
//	}
	public String getId() {
		return id;
	}
//	public void setId(String id) {
//		this.id = id;
//	}	
	
}
