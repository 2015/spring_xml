package com.zhxl.model.test;

import org.springframework.orm.hibernate4.HibernateTransactionManager;

public class TransactionManager {
	/**
	 * type:global and local
	 * local transaction special resource
	 * 
	 * spi:服务提供接口
	 * DataSourceTransactionManager属性性dataSource
	 * 
	 * Hibernate transaction
	 * LocalSessionFactoryBean
	 * 
	 * HibernateTransactionManager
	 * ref sessionFactory
	 * 
	 * SessionFactoryUtils
	 * DataSourceUtils
	 * 
	 * define transactionManager include property
	 * 
	 * define sevice impl
	 * 
	 * define transactionManager Strategy
	 * 
	 * define transactionManager config include point and <aop:advisor>
	 * 
	 * 
	 * 注解配置
	 * @Transactional类上
	 * 
	 * @Repository标注DAO层代码
	 * 持久化Resource依赖
	 * @AutoWired：Hibernate，DataSource
	 * @Inject  
	 * @Resourec 
	 * @PersistenceContext:JPA
	 * JdbcTemplete类为属性，需要注入数据源DataSource支持原生sql查询语句
	 * 
	 * 
	 * <context:property-placeholder location="jdbc.properties"/>
	 * 
	 * org.springframework.jdbc.datasource.DriverManagerDataSource
	 * 
	 * The older style of using Spring’s DAO templates is no longer recommended
	 * 
	 */
	private HibernateTransactionManager htm;		

}
