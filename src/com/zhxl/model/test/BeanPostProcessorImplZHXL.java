package com.zhxl.model.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;


public class BeanPostProcessorImplZHXL implements BeanPostProcessor,Ordered {
	Map<String,Object> map = new HashMap<String,Object>();

	@Override
	public Object postProcessAfterInitialization(Object paramObject,
			String paramString) throws BeansException {
		map.put("after1",paramObject);
		map.put("after2",paramString);
		return map;
	}

	@Override
	public Object postProcessBeforeInitialization(Object paramObject,
			String paramString) throws BeansException {
		map.put("before1",paramObject);
		map.put("before2",paramString);
		return map;
	}

	@Override
	public int getOrder() {
		return 0;
	}
	public Map<String,Object> getMap(){
		return this.map;
	}

}
