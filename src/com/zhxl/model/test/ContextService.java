package com.zhxl.model.test;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.zhxl.model.Internal.User;

@RunWith(SpringJUnit4ClassRunner.class)
/**
 * 使用下面的配置，applicationContext.xml一定需要在source folder目录
 * 否则会出现找不到xml文件，无法加载application或者出现bean名异常信息
 * 解决办法需要build path --- Use as source Folder
 */

/*
 * core ioc
 * beans DI
 * spring -context -support integrating third lib
 * supported jdk8
 * allow groovy define configuration file
 * servlet 3.0
 * @AutoWired support general type annotation
 * 
 * WepApplicationContext for web application
 * 
 * xml configuration metadata with xml or annotation
 * 
 * ClassPathXmlApplicationContext
 * FileSystemXmlApplicationContext
 * 
 * configuration header beans context
 * 
 * load other xml configuration 
 * <beans>
 * <import resource="service.xml"/>
 * </beans>
 * 
 * c namespace used
 * 
 * annotation @Resource for beans or fields on setter method
 * and by default name injected
 * Resource(name="id值")
 * 
 * in WebAppclicationContext use annotation not xml
 * need to config <context:component-scan base-package="org.example"/>
 * 
 * @Qualifier("public")限定注解
 * 
 * @Bean是一個方法級別的
 * 
 * You can use the @Bean annotation in a @Configuration-annotated or in a @Component-annotated
 * @Bean define init-mthod or destory-method and so on
 * 
 */
@ContextConfiguration(locations={"classpath:applicationContext.xml"})

//使用下面的方法是肯定行的,xml文件中需要执行bean的全路径及其id值
//@ContextConfiguration(locations={"file:src/com/zhxl/resource/applicationContext.xml"})

@Transactional
public class ContextService extends HibernateTemplate {
	/*
	 * 	java.lang.IllegalArgumentException: 'sessionFactory' or 'hibernateTemplate' is required
	 */
	
	@Autowired
	@Qualifier(value="sessionFactory")
	public void setP(SessionFactory sessionFactory){
		super.setSessionFactory(sessionFactory);
	}
	@Inject
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	ApplicationContext context;
	
	@Autowired
	@Qualifier(value="queryContextServiceImpl")
	QueryContextServiceImpl service;
	
	@Test
	public void testServiceTransactionManager(){
		service = null;
		List<User> users = null;
		try{
			users = service.getContext();
		}
		catch(MyException e){
			e.getCause();
		}
		
		String name = users.get(0).getName();
		System.out.println(name);
	}
	
	/**
	 * 出现错误java.lang.IllegalArgumentException: Pointcut is not well-formed: expecting ')' 
	 * at character position 11 excution(* com.zhxl.controller.Internal.impl.*.*(..)))
	 * xml配置文件<aop:point 执行表达式语句是否正确
	 * 
	 * aspectjweaver.jar
	 * 
	 * not found org.aopalliance.intercept.methodinterceptor class
	 * add jar aopalliance.jar
	 */
	
	
	
	
	
	
	
	
	
	
	
	//@Test
	public void testHibernateTemplate(){
		System.out.println(hibernateTemplate==null);
		
		SessionFactory sf = hibernateTemplate.getSessionFactory();
		System.out.println(sf==null);
		Session session = sf.openSession();
		/**
		 * 使用Session session = getSessionFactory().getCurrentSession();报错Could not obtain transaction-synchronized
		 */
		//could not open connect access denied for user 
		//username or password 错误
		
		Transaction transaction = session.beginTransaction();
		/*
		 * 下面写法报错nested transactions not supportedat org.
		 * 事务嵌套异常
		 */
		//session.beginTransaction().begin();
		//session.beginTransaction().begin();
		/**
		 * org.hibernate.TypeMismatchException: Provided id of the wrong type 
		 * for class com.zhxl.model.Internal.User. Expected: class java.lang.String, got class java.lang.Integer 	
		 * at org.hibernate.event.internal.DefaultLoadEventListener.onLoad(DefaultLoadEventListe
		 * 
		 * 传值的id一定要与数据库中类型保持一致，下面的12
		 */
		User u = (User) session.get(User.class,"12");
		String name = u.getName();
		System.out.println(name);
		u.setName("User");
		String nid =  u.getId().getClass().getName();
		System.out.println(nid);
		
		User user = new User();
		user.setAge("1024");
		/**
		 * 这里的ID即使设置，数据库也不会保存它的值，使用持久化的uuid值
		 */
		//user.setId("1024");
		user.setName("chifan");
		session.save(user);
		
		transaction.commit();
		
	}
	
	
	//@Test	
	public void getContext() {
		System.out.println(getSessionFactory()==null);
		Session session = getSessionFactory().openSession();
		/**
		 * 使用Session session = getSessionFactory().getCurrentSession();报错Could not obtain transaction-synchronized
		 */
		//could not open connect access denied for user 
		//username or password 错误
		
		Transaction transaction = session.beginTransaction();
		/*
		 * 下面写法报错nested transactions not supportedat org.
		 * 事务嵌套异常
		 */
		//session.beginTransaction().begin();
		//session.beginTransaction().begin();
		User u = (User) session.get(User.class, 1);
		String name = u.getName();
		System.out.println(name);
		transaction.commit();
	}

}
