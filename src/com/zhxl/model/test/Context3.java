package com.zhxl.model.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zhxl.model.test.Zhxl;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/com/zhxl/resource/applicationContext.xml"})
public class Context3 {
	@Autowired
	ApplicationContext con;
	
	Zhxl zx;
	
	//@Test
	public void testProperty(){
	    zx = (Zhxl) con.getBean("zhxl");
		String str = zx.parse();
		System.out.println(str);
	}

}
