package com.zhxl.mvc.Service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.zhxl.model.Internal.User;
import com.zhxl.mvc.Dao.QueryContextDaoImpl;

@Service
public class QueryContextServiceImpl implements QueryContextService{
	public QueryContextServiceImpl(){}
	/**
	 * appear property not writeable or lack setter
	 * because not setup :生成setter和getter方法
	 */
	@Autowired
	QueryContextDaoImpl queryContextDaoImpl;
	
	public QueryContextDaoImpl getQueryContextDaoImpl() {
		return queryContextDaoImpl;
	}

	public void setQueryContextDaoImpl(QueryContextDaoImpl queryContextDaoImpl) {
		this.queryContextDaoImpl = queryContextDaoImpl;
	}

	//@Transactional
	public <T> List<User> getContext() {
		System.out.println(queryContextDaoImpl==null);
		return (List<User>) queryContextDaoImpl.getContext();
	}
}
