package com.zhxl.model.test;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringUtil implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext arg0) throws BeansException {
         applicationContext = arg0;
    }
    public static Object getObject(String id) {
         Object object = null;
        object = applicationContext.getBean(id);      
        return object;
     }
 }

